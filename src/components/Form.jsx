import { BiMessageSquareAdd } from 'react-icons/bi';

const Form = ({ setInputText, inputText, todos, setTodos, tab, setTab }) => {
  // 寫js 的地方

  const inputTextHandler = (event) => {
    setInputText(event.target.value)
  }
  const submitTodo = (event) => {
    event.preventDefault()

    setTodos([
      ...todos,
      {
        text: inputText,
        completed: false,
        id: Math.random() * 999
      }
    ])

    setInputText('')
  }

  const handleSelect = (event) => {
    console.log(event.target.value)
    setTab(event.target.value)
    console.log({tab})
  }

  return (
    <form>
      <input
        type="text"
        className='todo-input'
        value={inputText}
        onChange={inputTextHandler}
      />

      <button
        type='submit'
        className='todo-button'
        onClick={submitTodo}
      >
        <BiMessageSquareAdd />
      </button>

      <div className='select'>
        <select name="todos" onChange={handleSelect} value={tab}>
          <option value="all">全部</option>
          <option value="completed">已完成</option>
          <option value="uncompleted">待完成</option>
        </select>
      </div>
    </form>
  )
}


export default Form