import Todo from './Todo'
import { MdCheck, MdDeleteOutline } from 'react-icons/md'
import { FaEdit } from 'react-icons/fa'

const TodoList = ({todos, setTodos, filterTodos}) => {
  return (
    <div>
      <div>
          <dl>
            <dt>Update: <FaEdit/></dt>
            <dt>Completed: <MdCheck/></dt>
            <dt>Delete: <MdDeleteOutline/></dt>
          </dl>
        </div>
      <div className="todo-container">
        <div className="todo-list">
            {filterTodos?.map(todo => (
                <Todo
                    key={todo.id}
                    todos={todos}
                    setTodos={setTodos}
                    text={todo.text}
                    todo={todo}
                />
            ))}
        </div>
      </div>
    </div>
  )
}

export default TodoList
