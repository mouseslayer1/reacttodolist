import {useState} from 'react'
import { MdCheck, MdDeleteOutline } from 'react-icons/md'
import { FaEdit } from 'react-icons/fa'

const Todo = ({todos, setTodos, text, todo}) =>{

    const [isEditing, setIsEditing] = useState(false)
    const [inputText, setInputText] = useState(todo.text)

    //Set completed state to the todo
    const completeTodo = () => {
        setTodos(todos.map(item => {
            if(item.id === todo.id){
                return {
                    ...item,
                    completed : !item.completed
                }
            }
            return item
        })
        )
    }

    //Delete the Todo
    const deleteTodo = () => {
        console.log('text: ', todos.filter(el => el.id !== todo.id))
        setTodos(todos.filter(el => el.id !== todo.id))
    }

    //Update the Todo text
    const updateTodo = () => {
        setIsEditing(!isEditing)
    }

    const handleUpdate = (event) => {
        event.preventDefault()
        setTodos(todos.map(item => {
            if(item.id === todo.id){
                return {
                    ...item,
                    text : inputText
                }
            }
            return item
        })
        )
        updateTodo()
    }

    const handleChange = event => {
        setInputText(event.target.value);
      };

    let result;

    if (isEditing) {
        result = (
          <div className="todo">
            <form className="todo-item" onSubmit={handleUpdate}>
              <input onChange={handleChange} value={inputText} type="text" />
              <button>Save</button>
            </form>
          </div>
        )
      } else {
        result = (
        <div className="todo">
            <li className={`todo-item ${todo.completed? 'completed' : ''}`}>
                {text}
            </li>
            <button className="edit-btn" onClick={updateTodo}>
            <FaEdit/>
            </button>
            <button className="complete-btn" onClick={completeTodo}>
            <MdCheck/>
            </button>
            <button className="trash-btn" onClick={deleteTodo}>
            <MdDeleteOutline/>
            </button>
        </div>
        )
      }

    return result
}

export default Todo