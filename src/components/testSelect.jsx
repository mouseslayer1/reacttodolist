import React from 'react'

const testSelect = () => {
  return (
    <div>
      <select>
        <option value="111">eee</option>
        <option value="222">fff</option>
      </select>
    </div>
  )
}

export default testSelect
