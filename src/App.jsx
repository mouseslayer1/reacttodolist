import {useState, useEffect} from 'react'
import './App.css'
import Form from './components/Form'
import TodoList from './components/TodoList'

function App() {
  //Define inputText for store todo text and method 'setInputText' which can update inputText
  const [inputText, setInputText] = useState('')

  //Define default array data of todos and method 'setTodos' which can update todos
  const [todos, setTodos] = useState([
    {text:'do homework', completed:false, id:1},
    {text:'do 222 dishes', completed:false, id:2},
    {text:'do 333 laundry', completed:false, id:3},
    {text:'make 444 dinner', completed:false, id:4},
    {text:'make 555 brunch', completed:false, id:5}
  ])

  //Define inputText for store todo text and method 'setInputText' which can update inputText
  const [tab, setTab] = useState('all')

  //Define inputText for store todo text and method 'setFilterTodos' which can update filterTodos
  const [filterTodos, setFilterTodos] = useState([])

  //Change the todos list display if tab changed
  const handleFilter = () => {
    console.log("app tab value:", tab)
    switch (tab) {
      case 'completed':
        setFilterTodos(todos.filter(todo => todo.completed))
        break
      case 'uncompleted':
        setFilterTodos(todos.filter(todo => !todo.completed))
        break
      default:
        setFilterTodos(todos)
        break
    }

  }

  //Triggered when tab or todos be changed
  useEffect(() => {
      handleFilter()
    }, [tab, todos])

  return (
    <div className="App">
      <div className="container">

        <header>
          Garrick's ToDoList
        </header>

 
        <Form
          inputText={inputText}
          setInputText={setInputText}
          todos = {todos}
          setTodos = {setTodos}
          setTab = {setTab}
          tab = {tab}
        />
        <TodoList 
          todos = {todos}
          setTodos = {setTodos}
          filterTodos = {filterTodos}
        />

      </div>

    </div>
  )
}

export default App